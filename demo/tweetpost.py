#-*-coding:utf-8-*-
import traceback
import logging
import time
import getpass
import base64

import requests
import bs4

headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36",
        }
def login(s, loginname, password):
    "login to twitter"
    login_post = "https://twitter.com/sessions"
    login_get = "https://twitter.com/login"
    r = s.get(login_get, headers=headers)
    soup = bs4.BeautifulSoup(r.text)
    loginform = soup.find("form", class_="clearfix signin js-signin")
    inputs = loginform.find_all("input")
    data=[]
    for input in inputs:
        name = input.get("name")
        if name:
            if "username" in name:
                value = loginname
            elif "password" in name:
                value = password
            else:
                value = input.get("value")
            data.append((name,value))

    r=s.post(login_post, data=data)
    if r.url != "https://twitter.com/" or r.status_code != 200:
        raise Exception("login failure of loginname:%s" % loginname)
def post(s, meg):
    "post tweet."
    index_url = "https://twitter.com/"
    create_url = "https://twitter.com/i/tweet/create"
    soup = bs4.BeautifulSoup(s.get(index_url).text)
    try:
        authenticity_token = soup.find("form", id="signout-form").find("input", class_="authenticity_token").get("value")
    except Exception, e:
        print traceback.format_exc()
        raise Exception("not found authenticity_token.")
    data = {}
    data["authenticity_token"] = authenticity_token
    data["place_id"] = ""
    data["status"] = meg
    print data

    r = s.post(create_url, data=data)
    print r.status_code
    print r.text

def post_pic(s, meg, pic):
    "post tweet with pic"
    index_url = "https://twitter.com/"
    upload_url = "https://upload.twitter.com/i/tweet/create_with_media.iframe"

    soup = bs4.BeautifulSoup(s.get(index_url).text)
    try:
        authenticity_token = soup.find("form", id="signout-form").find("input", class_="authenticity_token").get("value")
    except Exception, e:
        logging.error(traceback.format_exc())
        raise Exception("not found authenticity_token.")
    data = {}
    data["post_authenticity_token"] = authenticity_token
    data["iframe_callback"] = "window.top.swift_tweetbox_" + str(int(time.time()*1000))
    data["in_reply_to_status_id"] = ""
    data["impression_id"] = ""
    data["earned"] = ""
    data["page_context"] = ""
    data["status"] = meg
    data["media_data[]"] = base64.b64encode(open(pic, "rb").read())
    data["place_id"] = ""

    files = {"media_empty":("", "", "application/octet-stream")}

    r = s.post(upload_url, data=data, files=files)
    print r.status_code, r.text


if __name__ == "__main__":
    s = requests.Session()

    loginname= raw_input("username or email login to Twitter: ")
    password = getpass.getpass()
    meg = raw_input("input tweet you want to post: ")
    pic = raw_input("input pic path you want to upload: ")
    login(s, loginname, password)
    if not pic:
        post(s, meg)
    else:
        post_pic(s, meg, pic)



