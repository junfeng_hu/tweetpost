要求
====
语言环境
Python 2.7

第三方包
* lxml
* requests
* bs4
* tornado
* MySQL-python
* apscheduler
* sqlalchemy
* supervisor
* beautifulsoup4

无**GFW**

安装
====
pip install --upgrade -r requirements.txt

使用
====
python server.py [OPTIONS]

Options:

  --debug                           (default False)
  --help                           show this help information
  --port                            (default 8888)


