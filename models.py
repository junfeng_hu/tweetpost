# coding: utf-8
from sqlalchemy import create_engine


from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

from sqlalchemy import Column, Integer, String, DateTime, TEXT
class TwittersInfo(Base):
    __tablename__ = "twittersInfo"
    uid = Column(Integer, primary_key=True)
    userID = Column(String)
    text = Column(TEXT)
    subTime = Column(DateTime)
    userName = Column(String)
    passWord = Column(String)
    follows = Column(TEXT)
    img = Column(String)
    status = Column(Integer)
    timeZoneOffset = Column(String)



def create_tables(engine):
    Base.metadata.create_all(engine)

if __name__ == "__main__":
    import settings
    dburl = "postgresql://{0}:{1}@localhost/{2}".format(
            settings.DBUSER, settings.DBPASS, settings.DBNAME
            )
    dburl = "mysql://{0}:{1}@localhost/{2}".format(
            settings.DBUSER, settings.DBPASS, settings.DBNAME
            )
    engine = create_engine(dburl, echo=True)
    create_tables(engine)
