#-*-coding:utf-8-*-
import logging

from apscheduler.scheduler import Scheduler

import tornado.ioloop
import tornado.web
from tornado.options import define, options, parse_command_line

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

import requests

from request import login_twitter
from sched import runperiod
import settings

define('debug', default=settings.DEBUG, type=bool)
define('port', default=settings.PORT, type=int)

dburl = "postgresql://{0}:{1}@localhost/{2}".format(
        settings.DBUSER, settings.DBPASS, settings.DBNAME
        )
dburl = "mysql://{0}:{1}@localhost/{2}".format(
        settings.DBUSER, settings.DBPASS, settings.DBNAME
        )
import MySQLdb
def conn_func():
    conn = MySQLdb.connect(
            user = settings.DBUSER,
            passwd = settings.DBPASS,
            db = settings.DBNAME,
            port = settings.DBPORT,
            charset = "utf8"
            )
    return conn

class CheckUserHandler(tornado.web.RequestHandler):

    def post(self):
        loginname = self.get_body_argument("loginname", default="")
        password = self.get_body_argument("password", default="")
        if not loginname or not password:
            raise tornado.web.HTTPError(400)
        s = requests.Session()
        rep = login_twitter(s, loginname, password, "for checkuser", None)
        self.write(rep)

if __name__ == "__main__":
    logging.getLogger("apscheduler.scheduler").setLevel(logging.ERROR)
    logging.getLogger("apscheduler.threadpool").setLevel(logging.ERROR)
    logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging.ERROR)
    parse_command_line()
    application = tornado.web.Application(
            [
                (r"/checkuser", CheckUserHandler),
                ],
            debug = options.debug,
            )

    sched = Scheduler(coalesce=True, misfire_grace_time=7)
    period_job = sched.add_interval_job(runperiod, seconds=7, args=[conn_func, sched])
    try:
        logging.info("sched start...")
        sched.start()

        application.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()

    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().close()
        sched.unschedule_job(period_job)
        logging.info("shutdown...")
        sched.shutdown(wait=False)


