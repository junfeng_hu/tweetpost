#-*-coding:utf-8-*-
from apscheduler.scheduler import Scheduler
import requests


from sqlalchemy import and_
import traceback
import logging
import os.path
from datetime import datetime, timedelta

from MySQLdb.cursors import DictCursor

ADD = 1
RUNNING = 2
SUCCESS = 3

from request import login_twitter, post_tweet, post_tweet_with_pic
from models import TwittersInfo

def tweet_task(task, conn_func):
    logging.info("start tweet_task, uid: {uid}".format(uid=task["uid"]))
    conn = conn_func()
    cursor = conn.cursor(cursorclass=DictCursor)
    update_sql = '''UPDATE twittersInfo SET status=%s WHERE uid=%s;'''
    s = requests.Session()
    try:
        ret = login_twitter(s, task["userName"], task["passWord"], task["text"], task["subTime"])

        if (not ret["rep"]):
            raise Exception("user: %s login failed." %task["userName"])

        if (task["img"] and os.path.isfile(task["img"])):
            post_tweet_with_pic(s, task["userName"], task["passWord"], task["text"], task["subTime"], task["subTime"])
        else:
            post_tweet(s, task["userName"], task["passWord"], task["text"], task["subTime"])
        cursor.execute(update_sql, (SUCCESS, task["uid"]))
        logging.info("successfully tweeted, uid: {uid}".format(uid=task["uid"]))
    except Exception, e:
        cursor.execute(update_sql, (ADD, task["uid"]))
        logging.error(traceback.format_exc())
    conn.commit()
    cursor.close()
    conn.close()

def task_to_dict(task):
    d = {}
    d["uid"] = task.uid
    d["userID"] = task.userID
    d["text"] = task.text
    d["subTime"] = task.subTime
    d["userName"] = task.userName
    d["passWord"] = task.passWord
    d["follows"] = task.follows
    d["img"] = task.img
    d["status"] = task.status
    d["timeZoneOffset"] = task.timeZoneOffset
    return d

def runperiod(conn_func, sched):
    now = datetime.now()
    conn = conn_func()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute('''SELECT * FROM twittersInfo WHERE status=%s;''', (ADD,))
    tasks = cursor.fetchall()
    update_sql = '''UPDATE twittersInfo SET status=%s WHERE uid=%s;'''
    for i, task in enumerate(tasks, start=1):
        cursor.execute(update_sql, (RUNNING, task["uid"]))
        if task["subTime"] > now:
            sched.add_date_job(tweet_task, task["subTime"], args=[task, conn_func])
        if task["subTime"] <= now:
            sched.add_date_job(tweet_task, datetime.now()+timedelta(seconds=i), args=[task, conn_func])
    conn.commit()
    cursor.close()
    conn.close()

if __name__ == "__main__":

    '''
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    engine = create_engine("sqlite:///tasks.db", echo=True)
    Session = sessionmaker(bind=engine)
    session = Session()

    #logging.basicConfig(level = logging.INFO, format = '%(asctime)-12s %(levelname)-8s %(message)s',
            datefmt = '%m-%d %H:%M')
    sched = Scheduler(standalone=True)

    sched.add_interval_job(runperiod, seconds=7, args=[session])

    logging.info("sched start...")
    sched.start()
    logging.info("shutdown...")
    sched.shutdown()
    '''
    pass



